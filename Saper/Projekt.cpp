﻿//Damian Lubaszka,  Saper Projekt 

#include "framework.h"
#include "Projekt.h"
#include <random>
#include <time.h>
#define MAX_LOADSTRING 100
#define SIZE_CHILD 25
#define GAP 1
#define TITLE_BAR 30
#define X_MAX 30    
#define Y_MAX 24

struct Pole {
    HWND window;
    bool Bomb;
    bool Flag;
    int Bombs_around;
    bool Interaction;
};

//--------------//ZMIENNE GLOBALNE//--------------//
int X_SIZE;                                         //Wymiary aplikacji okienkowej 
int Y_SIZE;
double TIMER = 0;                                   //Obsluga Timera
bool TIMER_FIRST_TIME = true;
bool GAME = true;                                   //Obsluga Logiki Sapera
bool DEBUG = false;
bool RESET = false;
bool HIT_BOMB = false;
bool MESSAGE = true;
int bombs_start_up = 10;                            //Do stanu gry
int bombs = 10;
int X_WINDOW_COUNT = 10;
int Y_WINDOW_COUNT = 10;
int INTERACTION_COUNTER;
int CORRECT_FLAG;

//Do kolorowania 
const COLORREF NUMBER_COLORS[] = { 
    RGB(0, 0, 255), RGB(0, 127, 0), RGB(255, 0, 0), RGB(0, 0, 127),
    RGB(127, 0, 0), RGB(0, 127, 127), RGB(0, 0, 0), RGB(127, 127, 127)};

HWND parent;                                       //Wskazanie na glowne okno
HINSTANCE hInst;                                   //Obecna Instancja
WCHAR szTitle[MAX_LOADSTRING];                     //Pasek Tytulowy 
WCHAR szWindowClass[MAX_LOADSTRING];               //Nazwa klasy glownego okna
static Pole Field[X_MAX][Y_MAX];                   //Tablica do gry

//--------------//NAGLOWKI FUNKCJI//--------------//
ATOM                RegisterParent(HINSTANCE hInstance);
ATOM                RegisterChild(HINSTANCE hInstance);
BOOL                InitParent(HINSTANCE  hInstance, int nCmdShow);
BOOL                InitChildI(HINSTANCE hInstance, int nCmdShow, HWND hWnd, int X_SHIFT, int Y_SHIFT, int x, int y);
LRESULT CALLBACK    WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK    Child(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK    Message_Box(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam); 
INT_PTR CALLBACK    Set_Board(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);       

void                startGame(int bombs);
void                DisplayTitleBar();
void                How_many_bombs_on_Field();
void                GetPosition(HWND hWnd, int& X_pos, int& Y_pos);
void                Draw_Flag(HWND hWnd);
void                Erase_Flag(HWND hWnd);
void                DrawBomb(HWND hWnd, int X_pos, int Y_pos);
void                Draw_Number(HWND hWnd, int X_pos, int Y_pos, int R, int G, int B);
void                Draw_Field_Debug();
void                Draw_Field();
void                ChangeBackGround(HWND hWnd, int R, int G, int B, bool  TitleBar);
void                Reveal(int X_pos, int Y_pos);
void                Draw_Field_Debug_ij(int i, int j);
void                Draw_Field_ij(int i, int j);

//---------------//FUNKCJA GLOWNA//---------------//
int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance,
                      _In_ LPWSTR    lpCmdLine, _In_ int       nCmdShow) {
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PROJEKT, szWindowClass, MAX_LOADSTRING);
    RegisterParent(hInstance);
    RegisterChild(hInstance);
    srand((unsigned int)time(NULL));
    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJEKT));
    MSG msg;
    while (GAME) {
        startGame(bombs);
        if (!InitParent(hInstance, nCmdShow)) return FALSE;
        if (DEBUG) {
            CheckMenuItem(GetMenu(parent), IDM_HELP_DEBUG, MF_CHECKED);
            Draw_Field_Debug();
        }
        while (GetMessage(&msg, nullptr, 0, 0)) {
            if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
            if (INTERACTION_COUNTER == bombs_start_up || (CORRECT_FLAG == bombs_start_up && CORRECT_FLAG == (bombs_start_up - bombs))) {
                KillTimer(parent, 1);
                HIT_BOMB = true;
                if (MESSAGE) {
                    MessageBox(GetParent(parent), _T("WYGRANA!!!"), _T("SAPER"), MB_OK );
                    MESSAGE = false;
                }
            }
        }
    }
    return 0;
}

ATOM RegisterParent(HINSTANCE hInstance) {
    WNDCLASSEXW wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDC_PROJEKT));
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_PROJEKT);
    wcex.lpszClassName = szWindowClass;
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
    return RegisterClassExW(&wcex);
}

ATOM RegisterChild(HINSTANCE hInstance) {
    WNDCLASSEXW wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = Child;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = NULL;
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_GRAYTEXT+1);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = L"childClass";
    wcex.hIconSm = NULL;
    return RegisterClassExW(&wcex);
}

BOOL InitParent(HINSTANCE hInstance, int nCmdShow) {
    hInst = hInstance;
    RECT rc,R = { 0,0,X_SIZE,Y_SIZE };
    AdjustWindowRect(&R, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, TRUE);
    X_SIZE = R.right - R.left;
    Y_SIZE = R.bottom - R.top;
    HWND hWnd = CreateWindowW(szWindowClass, L"Saper", WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
        0, 0, X_SIZE, Y_SIZE, nullptr, nullptr, hInstance, nullptr);
    if (!hWnd) return FALSE;
    else parent = hWnd;
    SetWindowPos(hWnd, NULL, 0, 0, 0, 0,  SWP_NOSIZE);
    SystemParametersInfo(SPI_GETWORKAREA, 0, &rc, 0);
    MoveWindow(hWnd, (rc.left + rc.right + 1 - X_SIZE) / 2, (rc.top + rc.bottom + 1 - Y_SIZE) / 2, X_SIZE, Y_SIZE, TRUE);
    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);
    return TRUE;
}

BOOL InitChildI(HINSTANCE hInstance, int nCmdShow, HWND hWnd, int X_SHIFT, int Y_SHIFT, int i ,int j) {
    RECT rc;
    GetClientRect(hWnd, &rc);
    HWND child = CreateWindowW(L"childClass", L"child", WS_CHILD | WS_VISIBLE, rc.left + X_SHIFT, rc.top + Y_SHIFT, SIZE_CHILD, SIZE_CHILD, hWnd, nullptr, hInst, nullptr);
    if (!hWnd)  return FALSE;
    Field[i][j].window=child;
    ShowWindow(child, nCmdShow);
    UpdateWindow(child);
    return TRUE;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    switch (message)
    {
        case WM_CREATE:
        {
            for (int i = 0; i < Y_WINDOW_COUNT; i++)
                for (int j = 0; j < X_WINDOW_COUNT; j++)
                    InitChildI(hInst, 1, hWnd, j * (SIZE_CHILD+GAP), TITLE_BAR + i * (SIZE_CHILD + GAP), j, i);
        }
        break;
        
        case WM_GETMINMAXINFO:
        {
            MINMAXINFO* minMaxInfo = (MINMAXINFO*)lParam;
            minMaxInfo->ptMinTrackSize.x = minMaxInfo->ptMaxSize.x = minMaxInfo->ptMaxTrackSize.x = X_SIZE;
            minMaxInfo->ptMinTrackSize.y = minMaxInfo->ptMaxSize.y = minMaxInfo->ptMaxTrackSize.y = Y_SIZE;
        }
        break;
        
        case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            switch (wmId)
            {
                case IDM_ABOUT:
                    DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, Message_Box);
                break;
                
                case IDM_HELP_DEBUG:
                    if (DEBUG) {
                        CheckMenuItem(GetMenu(hWnd), IDM_HELP_DEBUG, MF_UNCHECKED);
                        DEBUG = false;
                        Draw_Field();
                    }
                    else {
                        DEBUG = true;
                        CheckMenuItem(GetMenu(hWnd), IDM_HELP_DEBUG, MF_CHECKED);
                        Draw_Field_Debug();
                    }
                break;
                
                case IDM_EXIT:
                    GAME = false;
                    DestroyWindow(hWnd);
                break;

                case IDM_CUSTOM_SIZE:
                    DialogBox(hInst, MAKEINTRESOURCE(IDD_BOARD), hWnd, Set_Board);
                    if (RESET) {
                        DestroyWindow(hWnd);
                        RESET = false;
                    }
                break;

                case IDM_NEW_GAME:
                    bombs = bombs_start_up;
                    DestroyWindow(hWnd);
                break;

                default: 
                    {
                    GAME = false;
                    return DefWindowProc(hWnd, message, wParam, lParam);
                    }
            }
        }
        break;
    
        case WM_ACTIVATE: 
        {
            Draw_Field();
            if (DEBUG) Draw_Field_Debug();
            DisplayTitleBar();
        }
        break;

        case WM_CLOSE:
            DestroyWindow(hWnd);
            GAME = false;
        break;

        case WM_TIMER:
        {
            if (wParam == 1) {
                TIMER += (0.1);
                DisplayTitleBar();
            }
        }
        break;
        
        case WM_PAINT:
        {
            DisplayTitleBar();
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            EndPaint(hWnd, &ps);
        }
        break;

        case WM_DESTROY:
            PostQuitMessage(0);
        break;

        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK Child(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    switch (message)
        {
        case WM_LBUTTONDOWN:
        {
            if (!HIT_BOMB) {
                if (TIMER_FIRST_TIME) {
                    SetTimer(parent, 1, 100, NULL);
                    TIMER_FIRST_TIME = false;
                }
                int i = 0, j = 0;
                GetPosition(hWnd, i, j);
                if (Field[i][j].Flag) break;
                if (Field[i][j].Interaction) {
                    ChangeBackGround(hWnd, 200, 200, 200, false);
                    if (Field[i][j].Bomb == true) {
                        KillTimer(parent, 1);
                        DrawBomb(hWnd, i, j);
                        HIT_BOMB = true;
                        MessageBox(GetParent(hWnd), _T("BOOOOM!"), _T("MINA"), MB_OK | MB_ICONERROR);
                        MessageBox(GetParent(hWnd), _T("Przegrana :("), _T("SAPER"), MB_OK);
                        Field[i][j].Interaction = false;
                        Draw_Field();
                        if (DEBUG) Draw_Field_Debug();
                    }
                    else {
                        if (Field[i][j].Bombs_around != 0) {
                            Draw_Number(hWnd, i, j, 200, 200, 200);
                            Field[i][j].Interaction = false;
                            INTERACTION_COUNTER--;
                        }
                        else Reveal(i, j);
                    }
                }
            }
        }
        break;

        case WM_RBUTTONDOWN:
        {
            if (!HIT_BOMB) {
                if (TIMER_FIRST_TIME) {
                    SetTimer(parent, 1, 100, NULL);
                    TIMER_FIRST_TIME = false;
                }
                int i = 0, j = 0;
                GetPosition(hWnd, i, j);
                if (Field[i][j].Interaction) {
                    if (Field[i][j].Flag) {
                        Field[i][j].Flag = false;
                        Erase_Flag(hWnd);
                        if (Field[i][j].Bomb == true) CORRECT_FLAG--;
                        bombs++;
                        if (DEBUG) {
                            if (Field[i][j].Bomb) DrawBomb(Field[i][j].window, i, j);
                            if (Field[i][j].Bombs_around != 0) Draw_Number(Field[i][j].window, i, j, 109, 109, 109);
                        }
                    }
                    else {
                        Field[i][j].Flag = true;
                        Draw_Flag(hWnd);
                        if (Field[i][j].Bomb == true) CORRECT_FLAG++;
                        bombs--;
                        DisplayTitleBar();
                    }
                }
            }
        }
        break;
        
        case WM_ERASEBKGND: 
            return -1;
        break;
        
        case WM_PAINT:
        {
            int i = 0, j = 0;
            GetPosition(hWnd, i, j);
            Draw_Field_ij(i,j);
            if (DEBUG) Draw_Field_Debug_ij(i,j);
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            EndPaint(hWnd, &ps);
        }
        break;

        case WM_DESTROY:
            PostQuitMessage(0);
        break;

        default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}
INT_PTR CALLBACK Message_Box(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
        case WM_INITDIALOG:
            return (INT_PTR)TRUE;

        case WM_COMMAND:
            if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) {
                EndDialog(hDlg, LOWORD(wParam));
                return (INT_PTR)TRUE;
            }
            break;
    }
    return (INT_PTR)FALSE;
}

INT_PTR CALLBACK Set_Board(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
        case WM_INITDIALOG:
            SetDlgItemInt(hDlg, IDC_EDIT1, Y_WINDOW_COUNT, TRUE);
            SetDlgItemInt(hDlg, IDC_EDIT2, X_WINDOW_COUNT, TRUE);
            SetDlgItemInt(hDlg, IDC_EDIT3, bombs_start_up, TRUE);
            return (INT_PTR)TRUE;

        case WM_COMMAND:

            if (LOWORD(wParam) == CS_CANCEL) {
                EndDialog(hDlg, LOWORD(wParam));
                return (INT_PTR)TRUE;
                RESET = false;
            }
            else if (LOWORD(wParam) == CS_OK) {   
                RESET = true;
                Y_WINDOW_COUNT = GetDlgItemInt(hDlg, IDC_EDIT1, NULL, TRUE);
                X_WINDOW_COUNT = GetDlgItemInt(hDlg, IDC_EDIT2, NULL, TRUE);
                bombs = GetDlgItemInt(hDlg, IDC_EDIT3, NULL, TRUE);
                if (Y_WINDOW_COUNT < 10 || X_WINDOW_COUNT< 10) {
                    Y_WINDOW_COUNT = 10;
                    X_WINDOW_COUNT = 10;
                    bombs = 10;
                }
                else {
                    if (Y_WINDOW_COUNT > Y_MAX) Y_WINDOW_COUNT = Y_MAX;
                    if (X_WINDOW_COUNT > X_MAX) X_WINDOW_COUNT = X_MAX;
                    if (0.8 * Y_WINDOW_COUNT * X_WINDOW_COUNT < bombs) bombs =(int)( 0.8 * Y_WINDOW_COUNT * X_WINDOW_COUNT);
                    if (bombs == 0) bombs = 1;
                }
                EndDialog(hDlg, LOWORD(wParam));
                return (INT_PTR)TRUE;
            }
            break;
    }
    return (INT_PTR)FALSE;
}

void startGame(int bombs) {
    X_SIZE = X_WINDOW_COUNT * SIZE_CHILD + X_WINDOW_COUNT - 1;      //Przygotowanie okna 
    Y_SIZE = Y_WINDOW_COUNT * SIZE_CHILD + TITLE_BAR + Y_WINDOW_COUNT - 1;
    TIMER = 0;                                                      //Przygotowanie timera
    TIMER_FIRST_TIME = true;
    HIT_BOMB = false;                                               //Przygotowanie stanu gry
    INTERACTION_COUNTER = X_WINDOW_COUNT * Y_WINDOW_COUNT;
    CORRECT_FLAG = 0;
    MESSAGE = true;
    bombs_start_up = bombs;
    for (int i = 0; i < X_MAX; i++)                                 //Przygotowanie tablicy gry
        for (int j = 0; j < Y_MAX; j++)
        {
            Field[i][j].Bomb = false;
            Field[i][j].Flag = false;
            Field[i][j].Bombs_around = 0;
            Field[i][j].Interaction = true;
        }
    int X_POS, Y_POS, local_counter = bombs;                        //Losowanie bomb
    while (local_counter > 0) {
        X_POS = rand() % X_WINDOW_COUNT;
        Y_POS = rand() % Y_WINDOW_COUNT;
        if (Field[X_POS][Y_POS].Bomb==false) {
            local_counter--;
            Field[X_POS][Y_POS].Bomb = true;
            }
    }
    How_many_bombs_on_Field();                                      //Zapisanie liczby ile bomb jest wokol
}

void How_many_bombs_on_Field() {
    for (int i = 0; i < X_WINDOW_COUNT; i++)
        for (int j = 0; j < Y_WINDOW_COUNT; j++)
        {
            if (Field[i][j].Bomb) continue;
            if (i - 1 >=0 && j - 1 >=0 && Field[i - 1][j - 1].Bomb==true) Field[i][j].Bombs_around++;
            if (i - 1 >= 0 &&  Field[i - 1][j].Bomb == true) Field[i][j].Bombs_around++;
            if (i - 1 >= 0 && j + 1 < Y_WINDOW_COUNT && Field[i - 1][j + 1].Bomb == true) Field[i][j].Bombs_around++;
            if (j - 1 >= 0 && Field[i][j - 1].Bomb == true) Field[i][j].Bombs_around++;
            if (j + 1 < Y_WINDOW_COUNT && Field[i][j + 1].Bomb == true) Field[i][j].Bombs_around++;
            if (i + 1 < X_WINDOW_COUNT && j - 1 >= 0 && Field[i + 1][j - 1].Bomb == true) Field[i][j].Bombs_around++;
            if (i + 1 < X_WINDOW_COUNT && Field[i + 1][j].Bomb == true) Field[i][j].Bombs_around++;
            if (i + 1 < X_WINDOW_COUNT && j + 1 < Y_WINDOW_COUNT &&  Field[i + 1][j + 1].Bomb == true) Field[i][j].Bombs_around++;
        }
}

void  GetPosition(HWND hWnd, int& X_pos, int& Y_pos) {
    for (int i = 0; i < X_WINDOW_COUNT; i++)
        for (int j = 0; j < Y_WINDOW_COUNT; j++)
            if (hWnd == Field[i][j].window) {
                X_pos = i;
                Y_pos = j;
                return; 
            }
}

void Draw_Flag(HWND hWnd) {
     HDC hdc = GetDC(hWnd);
     HBITMAP bitmap = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BITMAP1));
     HDC memDC = CreateCompatibleDC(hdc);
     HBITMAP oldBitmap = (HBITMAP)SelectObject(memDC, bitmap);
     BitBlt(hdc, 0, 0, 48, 48, memDC, 0, 0, SRCCOPY);
     RECT rc;
     GetClientRect(hWnd, &rc);
     StretchBlt(hdc, rc.left, rc.top, 25, 25, memDC, 0, 0, 48, 48, SRCCOPY);
     SelectObject(memDC, oldBitmap);
     DeleteObject(bitmap);
     DeleteDC(memDC);
     ReleaseDC(hWnd, hdc);
}

void Draw_Number(HWND hWnd, int X_pos, int Y_pos, int R, int G, int B) {
    HDC hdc = GetDC(hWnd);
    TCHAR ss[256];
    _stprintf_s(ss, 256, _T("%d"), Field[X_pos][Y_pos].Bombs_around);
    SetTextColor(hdc, NUMBER_COLORS[Field[X_pos][Y_pos].Bombs_around-1]);
    RECT rc;
    GetClientRect(hWnd, &rc);
    SetBkColor(hdc,RGB(R, G, B));
    DrawText(hdc, ss, (int)_tcslen(ss), &rc, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
    ReleaseDC(hWnd, hdc);
}

void Erase_Flag(HWND hWnd) {
    HDC hdc = GetDC(hWnd);
    HBRUSH hcolor = CreateSolidBrush(RGB(109,109,109));
    HGDIOBJ holdbrush = SelectObject(hdc, hcolor);
    RECT rc;
    GetClientRect(hWnd, &rc);
    FillRect(hdc, &rc, hcolor);
    SelectObject(hdc, holdbrush);
    bool tbool = DeleteObject(hcolor);
    ReleaseDC(hWnd, hdc);
}

void  DisplayTitleBar() {
    TCHAR ss[256];
    _stprintf_s(ss, 256, _T("%06.1f "), TIMER);
    HDC hdc = GetDC(parent);
    HFONT font = CreateFont(25, 0, 0, 0, FW_BOLD, 0, 0, 0, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
                 CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY, VARIABLE_PITCH, TEXT("Arial"));
    SelectObject(hdc, font);
    SetTextColor(hdc, RGB(255, 0, 0));
    RECT rc;
    GetClientRect(parent, &rc);
    rc.right = rc.right / 2;
    rc.bottom = rc.top + TITLE_BAR;
    if (bombs < 0) ChangeBackGround(parent, 0, 0, 0, true);
    DrawText(hdc, ss, (int)_tcslen(ss), &rc, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
    rc.left = rc.right;
    rc.right = rc.left * 2;
    _stprintf_s(ss, 256, _T("%04d "), bombs);
    DrawText(hdc, ss, (int)_tcslen(ss), &rc, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
    DeleteObject(font);
    ReleaseDC(parent, hdc);
}

void DrawBomb(HWND hWnd, int X_pos, int Y_pos) {
     HDC hdc = GetDC ( hWnd ) ;
     HBRUSH brush = CreateSolidBrush(RGB(0, 0, 0));
     HBRUSH oldBrush = (HBRUSH)SelectObject(hdc, brush);
     RECT rc;
     GetClientRect(hWnd, &rc);
     int x = (rc.right )/2;
     int y = rc.bottom /2;
     const int rad = 10;
     Ellipse(hdc, x - rad, y - rad, x + rad, y + rad);
     SelectObject(hdc, oldBrush);
     DeleteObject(brush);
     ReleaseDC(hWnd, hdc);
}

void ChangeBackGround(HWND hWnd, int R, int G, int B, bool TitleBar ) {
    HDC hdc = GetDC(hWnd);
    HBRUSH hcolor;
    RECT rc;
    GetClientRect(hWnd, &rc);
    if (TitleBar) { 
        hcolor = (HBRUSH)(COLOR_WINDOW + 1); 
        rc.bottom = TITLE_BAR;
    }
    else  hcolor = CreateSolidBrush(RGB(R, G, B));
    HGDIOBJ holdbrush = SelectObject(hdc, hcolor);
    FillRect(hdc, &rc, hcolor);
    SelectObject(hdc, holdbrush);
    bool tbool = DeleteObject(hcolor);
    ReleaseDC(hWnd, hdc);
}

void Reveal(int i, int j){

    if (Field[i][j].Bombs_around == 0&& Field[i][j].Interaction)
    {
        if (Field[i][j].Flag) { Erase_Flag(Field[i][j].window); Field[i][j].Flag = false; bombs++; }
        ChangeBackGround(Field[i][j].window,200,200,200,false);
        Field[i][j].Interaction = false;
        INTERACTION_COUNTER--;
        if (i - 1 >= 0 && j - 1 >= 0) Reveal(i - 1, j - 1);
        if (i - 1 >= 0) Reveal(i - 1, j);
        if (i - 1 >= 0 && j + 1 < Y_WINDOW_COUNT) Reveal(i - 1, j+1);
        if (j - 1 >= 0) Reveal(i, j-1 );
        if (j + 1 < Y_WINDOW_COUNT) Reveal(i, j+1);
        if (i + 1 < X_WINDOW_COUNT && j - 1 >= 0) Reveal(i+1 , j-1);
        if (i + 1 < X_WINDOW_COUNT) Reveal(i + 1, j);
        if (i + 1 < X_WINDOW_COUNT && j + 1 < Y_WINDOW_COUNT) Reveal(i + 1, j + 1);
    }
    else if (Field[i][j].Interaction) {
        if (Field[i][j].Flag) { Erase_Flag(Field[i][j].window); Field[i][j].Flag = false; bombs++; }
        ChangeBackGround(Field[i][j].window, 200, 200, 200,false);
        Field[i][j].Interaction = false;
        INTERACTION_COUNTER--;
        if(Field[i][j].Bombs_around != 0) Draw_Number(Field[i][j].window,i,j,200,200,200);
    }
}

void Draw_Field_Debug() {
    for (int i = 0; i < X_WINDOW_COUNT; i++)
        for (int j = 0; j < Y_WINDOW_COUNT; j++)
            if (Field[i][j].Interaction == true && !Field[i][j].Flag) {
                if (Field[i][j].Bomb) DrawBomb(Field[i][j].window, i, j);
                else if (Field[i][j].Bombs_around != 0) Draw_Number(Field[i][j].window, i, j, 109, 109, 109);
            }
}

void Draw_Field() {
for (int i = 0; i < X_WINDOW_COUNT; i++)
    for (int j = 0; j < Y_WINDOW_COUNT; j++)    
        if (Field[i][j].Flag) Draw_Flag(Field[i][j].window);
        else if(Field[i][j].Interaction) ChangeBackGround(Field[i][j].window, 109, 109, 109, false);
        else {
            ChangeBackGround(Field[i][j].window, 200, 200, 200, false);
            if (Field[i][j].Bombs_around != 0) Draw_Number(Field[i][j].window, i, j, 200, 200, 200);
            else if (Field[i][j].Bomb) DrawBomb(Field[i][j].window, i, j);
        }   
}

void Draw_Field_Debug_ij(int i, int j) {
    if (Field[i][j].Interaction == true && !Field[i][j].Flag) {
        if (Field[i][j].Bomb) DrawBomb(Field[i][j].window, i, j);
        else if (Field[i][j].Bombs_around != 0) Draw_Number(Field[i][j].window, i, j, 109, 109, 109);
    }
}

void Draw_Field_ij(int i, int j) {
    if (Field[i][j].Flag) Draw_Flag(Field[i][j].window);
    else if (Field[i][j].Interaction) ChangeBackGround(Field[i][j].window, 109, 109, 109, false);
    else {
        ChangeBackGround(Field[i][j].window, 200, 200, 200, false);
        if (Field[i][j].Bombs_around != 0) Draw_Number(Field[i][j].window, i, j, 200, 200, 200);
        else if (Field[i][j].Bomb) DrawBomb(Field[i][j].window, i, j);
    }
}