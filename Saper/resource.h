﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Projekt.rc
//
#define IDC_MYICON                      2
#define CS_CANCEL                       2
#define IDD_PROJEKT_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_PROJEKT                     107
#define IDI_SMALL                       108
#define IDC_PROJEKT                     109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     130
#define IDD_BOARD                       130
#define IDB_BITMAP1                     131
#define IDD_WIN1                        135
#define IDC_EDIT1                       1004
#define IDC_EDIT2                       1005
#define IDC_EDIT3                       1006
#define CS_OK                           1007
#define IDCANCEL                        1009
#define ID_FILE_EXIT                    32771
#define ID_FILE_EXIT32772               32772
#define IDM_NEW_GAME                    32773
#define IDM_CUSTOM_SIZE                 32774
#define ID_HELP_DEBUG                   32775
#define IDM_HELP_DEBUG                  32776
#define ID_FILE_                        32781
#define ID_FILE_32782                   32782
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32783
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
